/**
 * @file
 * Defines Javascript behaviors for the cookies module.
 */

(function (Drupal) {
  'use strict';

  /**
   * Define defaults.
   */
  Drupal.behaviors.cookies_fb_pixel = {
    // id corresponding to the cookies_service.schema->id.
    id: 'fb_pixel',

    activate: function () {
      var script = document.getElementById('cookies_fb_pixel');
      if (script) {
        var content = script.innerHTML;
        var newScript = document.createElement('script');
        newScript.innerHTML = content;
        script.parentNode.replaceChild(newScript, script);
      }
    },

    attach: function (context) {
      var self = this;
      document.addEventListener('cookiesjsrUserConsent', function (event) {
        var service = (typeof event.detail.services === 'object') ? event.detail.services : {};
        if (typeof service[self.id] !== 'undefined' && service[self.id]) {
          self.activate(context);
        }
      });
    }
  }
})(Drupal);
